package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"net/http"
	"io"
	"io/ioutil"
	"math/big"
	"net"
	"time"
)

// get public key from pem
func GeneratePublicKeyFromPEM( publicKeyPEM string ) *rsa.PublicKey {
	block, _ := pem.Decode( [ ]byte( publicKeyPEM ) )
	publicKey, err := x509.ParsePKCS1PublicKey( block.Bytes )
	if err != nil {
		fmt.Println( "can't parse public key PEM:",
			err )
		return nil
	}
	return publicKey
}

// get public key from certificate pem
func GenerateCertificatePublicKeyFromPEM( publicKey string ) *rsa.PublicKey {
	block, _ := pem.Decode( [ ]byte( publicKey ) )
	certificate, err := x509.ParseCertificate( block.Bytes )
	if err != nil {
		fmt.Println( "can't parse certificate:",
			err )
		return nil
	}
	return certificate.PublicKey.(*rsa.PublicKey)
}

// get private key from pem
func GeneratePrivateKeyFromPEM( privateKey string ) *rsa.PrivateKey {
	block, _ := pem.Decode( [ ]byte( privateKey ) )
	private, err := x509.ParsePKCS1PrivateKey( block.Bytes )
	if err != nil {
		fmt.Println("can't parse private key:",
			err )
		return nil
	}
	return private
}

// encrypt data with public key (where the public key is pem public key)
func EncryptWithPEM( publicKeyPEM string,
	data [ ]byte ) [ ]byte {
	publicKey := GeneratePublicKeyFromPEM( publicKeyPEM )
	if publicKey == nil {
		return nil
	}
	return EncryptRSA( publicKey,
		data )
}

// encrypt
func EncryptRSA( publicKey *rsa.PublicKey,
	data [ ]byte ) [ ]byte {
	output, err := rsa.EncryptPKCS1v15( rand.Reader,
		publicKey,
		data )
	if err != nil {
		return nil
	}
	return output
}

// decrypt data with public key (where privateKey is the pem data)
func DecryptWithPEM( privateKeyPEM string,
	data [ ]byte ) [ ]byte {
	privateKey := GeneratePrivateKeyFromPEM( privateKeyPEM )
	if privateKey == nil {
		return nil
	}
	return DecryptRSA( privateKey,
		data )
}

// decrypt
func DecryptRSA( privateKey *rsa.PrivateKey,
	data [ ]byte ) [ ]byte {
	output, err := rsa.DecryptPKCS1v15( rand.Reader,
		privateKey,
		data )
	if err != nil {
		return nil
	}
	return output
}

// load private key from file
func LoadPrivateKeyFromFile( privateKeyFilePath string ) *rsa.PrivateKey {
	if fileContent, err := ioutil.ReadFile( privateKeyFilePath ); err == nil {
		if block, _ := pem.Decode( fileContent ); block != nil {
			if privateKey, err := x509.ParsePKCS1PrivateKey( block.Bytes ); err == nil {
				return privateKey
			}
		}
	}
	return nil
}

// export public key
func ExportPublicKey( publicKey rsa.PublicKey ) string {
	asn1Bytes, err := asn1.Marshal( publicKey )
	if err != nil {
		fmt.Println( "can't marshal rsa key:",
			err )
		return ""
	}
	var pemKey = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: asn1Bytes,
	}
	return string( pem.EncodeToMemory( pemKey ) )
}

// generate rsa where bitCount is a pow of 2
func GenerateRSA( bitCount int ) ( privateKey *rsa.PrivateKey, publicKeyPEM string ) {
	privateKey, err := rsa.GenerateKey( rand.Reader,
		bitCount )
	if err != nil {
		fmt.Println( "can't generate rsa key:",
			err )
		return nil, ""
	}
	return privateKey,
		ExportPublicKey( privateKey.PublicKey )
}

// generates TLS keypair for https server
func GenerateX509KeyPair( commonName string,
	country string,
	organization string,
	organizationUnit string,
	dayValidDuration int,
	subjectKeyID string ) ( *tls.Certificate, error ) {
	now := time.Now()
	template := &x509.Certificate{
		SerialNumber:           big.NewInt( now.Unix( ) ),
		Subject: pkix.Name{
			CommonName:         commonName,
			Country:            []string{ country },
			Organization:       []string{ organization },
			OrganizationalUnit: []string{ organizationUnit },
		},
		NotBefore:              now,
		NotAfter:               now.AddDate(0, 0, dayValidDuration ), // Valid for one day
		SubjectKeyId:           [ ]byte( subjectKeyID ),
		BasicConstraintsValid:  true,
		IsCA:                   true,
		ExtKeyUsage:            [ ]x509.ExtKeyUsage{ x509.ExtKeyUsageServerAuth },
		KeyUsage:               x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
	}

	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return &tls.Certificate{}, err
	}

	cert, err := x509.CreateCertificate(rand.Reader, template, template,
		priv.Public(), priv)
	if err != nil {
		return &tls.Certificate{}, err
	}

	outCert := &tls.Certificate{ }
	outCert.Certificate = append(outCert.Certificate, cert)
	outCert.PrivateKey = priv

	return outCert, nil
}

func GenerateDefaultX509KeyPair( ) ( *tls.Certificate, error ) {
	return GenerateX509KeyPair( "GhostButler",
		"FR",
		"GhostButler",
		"GhostButlerINC",
		365,
		"GhostButler")
}

// generate random symmetrical encryption key
func GenerateRandomSymmetricalKey( bitCount int ) [ ]byte {
	key := make( [ ]byte,
		bitCount,
		bitCount )
	rand.Read( key )
	return key
}

// todo for both encrypt/decrypt AES do not include IV into encrypted data but share it at key exchange
// encrypt AES
func EncryptAES( key, text [ ]byte ) ( [ ]byte, error ) {
	block, err := aes.NewCipher( key )
	if err != nil {
		return nil, err
	}
	b := base64.StdEncoding.EncodeToString( text )
	cipherText := make( [ ]byte, aes.BlockSize+len( b ) )
	iv := cipherText[ : aes.BlockSize ]
	if _, err := io.ReadFull( rand.Reader, iv ); err != nil {
		return nil, err
	}
	cfb := cipher.NewCFBEncrypter( block, iv )
	cfb.XORKeyStream( cipherText[ aes.BlockSize : ], [ ]byte( b ) )
	return cipherText, nil
}

// decrypt AES
func DecryptAES( key, text [ ]byte ) ( [ ]byte, error ) {
	block, err := aes.NewCipher( key )
	if err != nil {
		return nil, err
	}
	if len( text ) < aes.BlockSize {
		return nil, errors.New("cipher text too short" )
	}
	iv := text[ : aes.BlockSize ]
	text = text[ aes.BlockSize : ]
	cfb := cipher.NewCFBDecrypter( block,
		iv )
	cfb.XORKeyStream( text,
		text )
	data, err := base64.StdEncoding.DecodeString( string( text ) )
	if err != nil {
		return nil, err
	}
	return data, nil
}

// From https://golang.org/src/net/http/server.go
// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted
// connections. It's used by ListenAndServe and ListenAndServeTLS so
// dead TCP connections (e.g. closing laptop mid-download) eventually
// go away.
type tcpKeepAliveListener struct {
	*net.TCPListener
}

// accept a client
func ( ln tcpKeepAliveListener ) Accept( ) ( c net.Conn, err error ) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	tc.SetKeepAlive( true )
	tc.SetKeepAlivePeriod( 30 * time.Second )
	return tc,
		nil
}

// start a server using in-memory TLS KeyPair
func ListenAndServeTLSKeyPair( addr string,
	cert *tls.Certificate,
	handler http.Handler ) error {
	// build server
	server := &http.Server{
		Addr: addr,
		Handler: handler,
		ReadHeaderTimeout: time.Second * 5,
	}

	// configure tls
	config := &tls.Config{ }
	config.NextProtos = [ ]string{ "http/1.1", "http/2" }
	config.Certificates = make( [ ]tls.Certificate,
		1 )
	config.Certificates[ 0 ] = *cert

	// listen on given address
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	// build tls listener
	tlsListener := tls.NewListener( tcpKeepAliveListener{
			ln.( *net.TCPListener ),
		},
		config )

	// serve
	return server.Serve( tlsListener )
}

// calculate sha512 hash
func CalculateSHA512Hash( data [ ]byte ) string {
	// sha 512 handler
	s := sha512.New( )

	// write data
	s.Write( data )

	// output hash
	return hex.EncodeToString( s.Sum( nil ) )
}
