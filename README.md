Crypto
======

Introduction
------------

This is the crypto library for symmetrical/asymmetrical
encryption/decryption. It also provides a way to create an self-signed
certificate, and to start an HTTPS server with it.

Example
-------

A complete example of use can be found into
gitlab.com/ghostbutler/tool/service/common which create an HTTPS service.

Steps to create an HTTPS server are the following:

```go
// generate certificate
service.certificate, _ = crypto.GenerateDefaultX509KeyPair( )

// listen and serve
go crypto.ListenAndServeTLSKeyPair( ":" +
	strconv.Itoa( listeningPort ),
	service.certificate,
	service )
```

Where listeningPort is the port you want your server to listen on,
and service is a structure implementing the `http.Handler` interface.

For the other functions, they are basics and do not need example, read
the comments in the code.

Author
------

GhostButler <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/tool/crypto.git
